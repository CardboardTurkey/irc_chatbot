use std::process::ExitCode;

use anyhow::{Context, Result};
use futures::prelude::*;
use irc::client::prelude::*;
use regex::Regex;
use rust_bert::pipelines::conversation::{ConversationManager, ConversationModel};
use tokio;

async fn jump_on_irc(
    conversation_model: ConversationModel,
    mut conversation_manager: ConversationManager,
) -> Result<()> {
    let config = Config::load("config.toml")?;
    let nick = config.nickname()?.to_owned();
    let nick_search = format!(r"^\s*{nick}:?\s*");
    let mut client = Client::from_config(config).await?;
    client.identify()?;

    let mut stream = client.stream()?;
    while let Some(message) = stream.next().await.transpose()? {
        if let Command::PRIVMSG(ref channel, ref content) = message.command {
            if !Regex::new(&nick_search)?.is_match(content) {
                continue;
            }
            let convo = Regex::new(&nick_search)?.replace_all(content, "");
            log::info!("Responding to '{convo}'");
            let conversation_id = conversation_manager.create(&convo);
            let output = conversation_model
                .generate_responses(&mut conversation_manager)
                .get(&conversation_id)
                .context("Lol this shouldn't happen")?
                .clone();
            client.send_privmsg(channel, output)?;
        }
    }
    Ok(())
}

fn run() -> Result<()> {
    let conversation_model = ConversationModel::new(Default::default())?;
    let conversation_manager = ConversationManager::new();
    let rt = tokio::runtime::Runtime::new()?;
    rt.block_on(jump_on_irc(conversation_model, conversation_manager))?;
    Ok(())
}

fn main() -> ExitCode {
    env_logger::init();
    if let Err(err) = run() {
        log::error!("{err:?}");
        return ExitCode::FAILURE;
    }
    ExitCode::SUCCESS
}
